from django.urls import path


from . import views
urlpatterns = [
	# The path( functio receives four arguments)
	# We'll focus on 2 arguments that are required, 'route' and 'views'
	path('', views.index, name='index')
]